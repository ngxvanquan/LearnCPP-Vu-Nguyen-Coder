#include <iostream>
#include <string>

int mode = 0;

void choose_mode()
{
        std::cout << "***** TRINH QUAN LY SINH VIEN *****" << std::endl
            << "1. Them" << std::endl
            << "2. Sua" << std::endl
            << "3. Xoa" << std::endl
            << "4. Tim Kiem" << std::endl
            << "Vui Long Chon Che Do: ";
        std::cin >> mode;
}

void mode_active()
{
    if (mode == 1)
    {
        std::cout << std::endl << "Ban Da Chon Che Do Them Sinh Vien!" << std::endl;
	std::cout << std::endl;
        choose_mode();
    }
    else if (mode == 2)
    {
        std::cout << std::endl << "Ban Da Chon Che Do Sua Thong Tin Sinh Vien!" << std::endl;
	std::cout << std::endl;
        choose_mode();
    }
    else if (mode == 3)
    {
        std::cout << std::endl << "Ban Da Chon Che Do Them Sinh Vien!" << std::endl;
	std::cout << std::endl;
        choose_mode();
    }
    else if (mode == 4)
    {
        std::cout << std::endl << "Ban Da Chon Che Do Tim Kiem Sinh Vien!" << std::endl;
	std::cout << std::endl;
        choose_mode();
    }
    else
    {
        std::cout << std::endl << "Che Do Khong Ton Tai!" << std::endl;
	std::cout << std::endl;
        choose_mode();
    }
}

int main()
{
    while (1)
    {
        choose_mode();
        mode_active();
    }

    return 0;
}



