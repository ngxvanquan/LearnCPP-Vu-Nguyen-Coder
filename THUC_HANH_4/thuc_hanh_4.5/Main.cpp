#include <iostream>
#include <cmath>

int main()
{
	float a = 0, b = 0, c = 0;
	std::cout << "Cho Phuong Trinh ax^2 + bx + c = 0" << std::endl
		<< "Nhap Lan Luot 3 He So: a, b, c = ";
	std::cin >> a >> b >> c;

	int delta = (pow(b, 2) - 4 * a * c);
	if (a != 0)
	{
		if (delta == 0)
		{
			std::cout << "Phuong Trinh Co Nghiem Kep!" << std::endl
				<< "x = " << (-b / (2*a)) << std::endl;
		}
		else if (delta > 0)
		{
			std::cout << "Phuong Trinh Co Hai Nghiem!" << std::endl
				<< "x1 = " << ((-b + sqrt(delta)) / (2*a)) << std::endl
				<< "x2 = " << ((-b - sqrt(delta)) / (2*a)) << std::endl;
		}
		else 
		{
			std::cout << "Phuong Trinh Vo Nghiem!" << std::endl;
		}
	}
	else 
	{
		std::cout << "a khong the = 0" << std::endl;
	}


	return 0;
}
