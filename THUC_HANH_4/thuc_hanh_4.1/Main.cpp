#include <iostream>

int main()
{
	int x;
	int y;
	std::cout << "Nhap So Chia x = "; std::cin >> x;
	std::cout << "Nhap So Bi Chia y = "; std::cin >> y;
	if (x == 0 || y == 0)
	{
		std::cout << "Phep Chia Khong Hop Le!" << std::endl; 
	}
	else if (x % y != 0)
	{
		std::cout << x << " Chia Het Cho " << y << ". So Du La: " << x % y << std::endl;
	}
	else 
	{
		std::cout << x << " Chia Het Cho " << y << std::endl;
	}

}
