#include <iostream>

int main()
{
	int year = 0;
	std::cout << "Vui Long Nam = "; std::cin >> year;

	if (year % 400 == 0 || (year % 4 == 0 && year != 0))  
	{
		std::cout << year << " La nam Nhuan!" << std::endl;
	}
	else 
	{
		std::cout << year << " La Nam Thuong!" << std::endl;
	}

	return 0;
}
