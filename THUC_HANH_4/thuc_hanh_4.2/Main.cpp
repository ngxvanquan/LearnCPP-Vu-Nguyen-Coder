#include <iostream>

int main()
{
	int month = 0;
	std::cout << "Vui Long Nhap Nam = "; std::cin >> month;

	switch (month)
	{
		case 1:
		{
			std::cout << "Thang 1 Thuoc Quy 1 Va Co 31 Ngay!" << std::endl;	
			break;
		}
		case 2:
		{
			std::cout << "Thang 2 Thuoc Quy 1 Va Co 28 Hoac 29 Ngay!" << std::endl;
			break;
		}
		case 3:
		{
			std::cout << "Thang 3 Thuoc Quy 1 Va Co 31 Ngay!" << std::endl;
			break;
		}
		case 4:
		{
			std::cout << "Thang 4 Thuoc Quy 2 Va Co 30 Ngay!" << std::endl;
			break;
		}
		case 5:
		{
			std::cout << "Thang 5 Thuoc Quy 2 Va Co 31 Ngay!" << std::endl;
			break;
		}
		case 6:
		{
			std::cout << "Thang 6 Thuoc Quy 2 Va Co 30 Ngay!" << std::endl;
			break;
		}
		case 7:
		{
			std::cout << "Thang 7 Thuoc Quy 3 Va Co 31 Ngay!" << std::endl;
			break;
		}	
		case 8:
		{
			std::cout << "Thang 8 Thuoc Quy 3 Va Co 31 Ngay!" << std::endl;
			break;
		}
		case 9:
		{
			std::cout << "Thang 9 thuoc Quy 3 Cao Co 30 Ngay!" << std::endl;
			break;
		}
		case 10:
		{
			std::cout << "Thang 10 Thuoc Quy 4 Va Co 31 Ngay!" << std::endl;
			break;
		}
		case 11:
		{
			std::cout << "Thang 11 Thuoc Quy 4 Va Co 30 Ngay!" << std::endl;
			break;
		}
		case 12:
		{
			std::cout << "Thang 12 Thuoc Quy 4 Co Co 31 Ngay!" << std::endl;
			break;
		}
		default:
		{
			std::cout << "Thang Khong Ton Tai!" << std::endl;
		}
	}

	return 0;
}
