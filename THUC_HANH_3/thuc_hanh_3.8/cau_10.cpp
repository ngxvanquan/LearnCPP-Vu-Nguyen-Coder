#include <iostream>

int main()
{
	char c = 'b';
	bool b = (c < 'c') || !(c > 'a') && (c != 'b') && (c + 1 >= 'd');
	std::cout << b << std::endl;

	return 0;
}
