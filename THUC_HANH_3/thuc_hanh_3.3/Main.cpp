#include <iostream>
#include <cmath>

int main()
{
	float a = 0, b = 0, c = 0;
	std::cout << "Nhap Canh a = ";
	std::cin >> a;
	std::cout << "Nhap Canh b = ";
	std::cin >> b;
	std::cout << "Nhap Canh c = ";
	std::cin >> c;

	float p = (a+b+c) / 2;

	std::cout << std::endl << "Dien tich Tan Giac La: " << sqrt(p*(p - a)*(p - b)*(p - c)) << std::endl;

	return 0;
}
