#include <iostream>

int main()
{
	bool a = 0, b = 0;
	std::cout << "(a) Toi Muon Chen Ngoc Chinh: ";
	std::cin >> a;
	std::cout << "(b) Toi Muon Chen Maria Ozawa: ";
	std::cin >> b;

	bool c = a && b;
	bool d = a || b;
	bool e = a && !b;
	bool f = e || (!a && b);

	std::cout << std::endl << "Ket Qua:" << std::endl
		<< "(c) " << c << std::endl
		<< "(d) " << d << std::endl
		<< "(e) " << e << std::endl
		<< "(f) " << f << std::endl;

	return 0;
}
