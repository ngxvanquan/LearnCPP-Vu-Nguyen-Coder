#include <iostream>

int main()
{
	float x = 0;
	std::cout << "Vui Long Nhap x = ";
	std::cin >> x;

	bool a = x >= 0 && x <= 4;
	bool b = x >= 0 && x <= 6;
	bool c = x >= 0 && x <= 1;
	bool e = x <= 0 && x >= -2;

	std::cout << std::endl << "Ket Qua 1: " << a << std::endl
		<< "Ket Qua 2: " << b << std::endl
		<< "Ket Qua 3: " << c << std::endl
		<< "Ket Qua 4: " << e << std::endl;

	return 0;
}
