#include <iostream>
#include <cmath>

int main()
{
	float r = 0, pi = 3.14;
	std::cout << "Vui Long Nhap Ban Kinh Hinh Tron: ";
	std::cin >> r;
	std::cout << "Chu Vi Hinh Tron La: " << pi*(r*2) << std::endl 
		<< "Dien Tich Hinh Tron La: " << pow(r, 2) * pi << std::endl;

	return 0;
}
