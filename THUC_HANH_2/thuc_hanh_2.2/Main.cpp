#include <iostream>
#include <string>

int main()
{
	// Khai Bao Bien (declare variable)
	std::string ho_ten = " ";
	int nam_sinh = 0;
	std::string que_quan = " ";
	float chieu_cao = 0;
	float can_nang = 0;
	bool tinh_trang_hon_nhan = false;
	char xep_loai_tot_nghiep = ' ';

	// Lay Du Lieu Tu Console (get data from console)
	std::cout << "Nhap ho ten: ";
	std::getline(std::cin, ho_ten);
	std::cout << "Nhap nam sinh: ";
	std::cin >> nam_sinh;
	std::cout << "Nhap que quan: ";
	std::cin.ignore();
	std::getline(std::cin, que_quan);
	std::cout << "Nhap chieu cao: ";
	std::cin >> chieu_cao;
	std::cout << "Nhap can nang: ";
	std::cin >> can_nang;
	std::cout << "Nhap tinh trang hon nhan(1 - da ket hon; 0 - chua ket hon): ";
	std::cin >> tinh_trang_hon_nhan;
	std::cout << "Nhap xep_loai_tot_nghiep: ";
	std::cin >> xep_loai_tot_nghiep;
	std::cout << std::endl;

	// In ra console (print to console)
	std::cout << "**************************" << std::endl
		<< "***** SO YEU LY LICH *****" << std::endl
		<< "**************************" << std::endl;
	std::cout << "Ho ten: " << ho_ten << std::endl
		<< "Nam sinh: " << nam_sinh << std::endl
		<< "Que quan: " << que_quan << std::endl
		<< "Chieu cao: " << chieu_cao << " m" << std::endl
		<< "Can Nang: " << can_nang << " kg" << std::endl
		<< "Tinh trang hon nhan(1 - da ket hon; 0 - chua ket hon): " << tinh_trang_hon_nhan << std::endl
		<< "Xep_loai_tot_nghiep: " << xep_loai_tot_nghiep << std::endl;

	return 0;
}
