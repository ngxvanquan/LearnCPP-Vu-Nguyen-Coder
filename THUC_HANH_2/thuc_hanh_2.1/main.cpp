#include <iostream>
#include <string>

int main()
{
	// Khai Bao Bien
	int age = 16;
	std::string fullname = "Nguyen Van Quan";
	std::string contry = "Viet Nam";
	float weight = 40.2;

	//In Du Lieu Ra Console
	std::cout << "Xin chao. Toi la " << fullname << "." << std::endl
		<< "Toi den tu " << contry << "." << std::endl
		<< "Nam nay toi " << age << " tuoi" << std::endl
		<< "Toi nang " << weight << "kg" << std::endl;
}
